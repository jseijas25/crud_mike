import { CRUDMikePage } from './app.po';

describe('crud-mike App', () => {
  let page: CRUDMikePage;

  beforeEach(() => {
    page = new CRUDMikePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
