import { UpdateContactComponent } from './contacts/update-contact/update-contact.component';
import { AddContactComponent } from './contacts/add-contact/add-contact.component';
import { ContactsComponent } from './contacts/contacts.component';
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/contacts', pathMatch: 'full' },
  { path: 'contacts', component: ContactsComponent },
  { path: 'contacts/add', component: AddContactComponent },
  { path: 'contacts/update/:id', component: UpdateContactComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
