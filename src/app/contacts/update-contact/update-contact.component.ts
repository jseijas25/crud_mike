import { ContactService } from './../contact.service';
import { ActivatedRoute,Router } from '@angular/router';
import { contact } from './../contact';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-update-contact',
  templateUrl: './update-contact.component.html',
  styleUrls: ['./update-contact.component.css']
})
export class UpdateContactComponent implements OnInit , OnDestroy{
  id:any;
  params:any;
  //public contact: contact;
  contact = new contact();
  constructor(
    private route: ActivatedRoute,
    private contactService: ContactService,
    private router: Router
  ) { }

  ngOnInit() {
    this.params = this.route.params.subscribe(params => this.id = params['id']);
  //  const id = this.route.snapshot.paramMap.get('id');
    this.contactService.getContact(this.id).subscribe(
      contact =>{
        console.log(contact);
        this.contact.id=contact['id'];
        this.contact.name=contact['name'];
        this.contact.company=contact['company'];
        this.contact.email=contact['email'];
        this.contact.telephone=contact['telephone'];
        this.contact.notes=contact['notes'];
      },
    error => console.log(<any>error));
  }
  ngOnDestroy(){
    this.params.unsubscribe();
  }

  updateContact(contact){
    this.contactService.updateContact(contact).subscribe(
      contact => {console.log(contact);
      },
    error => console.log(<any>error));
  }
  /*update(contact: contact){
    this.userService.updateUser(contact).subscribe(() => {
      this.router.navigate(['/contacts']);
    })
  }*/
}
