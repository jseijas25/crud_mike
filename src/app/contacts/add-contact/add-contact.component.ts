import { Router } from '@angular/router';
import { ContactService } from './../contact.service';
import { contact } from './../contact';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent  {
  //contacts: contact[]=[];
  contact: contact = new contact;
  constructor(
    private contactservice: ContactService,
    private router: Router
  ){}

  save(contact){
    console.log(contact);
     this.contactservice.addContact(contact).subscribe(
      contact=> console.log(contact),
      error => console.log(<any>error)
    );
    this.router.navigate(['/contacts']);
  }


}
