import { ContactService } from './contact.service';
import { Component, OnInit,ViewChild } from '@angular/core';
import { contact } from './contact';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})

  export class ContactsComponent implements OnInit{
    public contacts: contact[];
    dataSource = new MatTableDataSource();
    displayedColumns = ['name', 'email', 'telephone','actions'];

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

    @ViewChild(MatSort) sort: MatSort;

    ngAfterViewInit() {
      this.dataSource.sort = this.sort;
    }

    msg: string = '';
    //hideUpdate: boolean = true;

    constructor(private userService: ContactService){


       }
     ngOnInit() {
      this.getUsers();
          this.dataSource = new MatTableDataSource(this.contacts);

    }

    getUsers(): void {
      this.userService.getUsers()
      .subscribe(contacts => {
        this.contacts = contacts;
        this.dataSource = new MatTableDataSource(this.contacts);
      });

    }

    deleteContact(i): void {
      var answer = confirm("estas seguro de querer eliminar?");
      if (answer) {
        this.userService.deleteContact(i)
         .subscribe(contacts => {
          console.log(i);

      }

    );
        this.contacts.splice(i,1);
        this.msg = 'Usuario eliminado exitosamente'
      }
    }


    closeAlert(): void {
      this.msg = ''
    }
  }
