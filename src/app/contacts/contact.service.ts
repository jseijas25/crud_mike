
import { contact } from './contact';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {Http, Headers, Response} from '@angular/http';




const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ContactService {
   headers: Headers = new Headers;
  private usersUrl = 'http://127.0.0.1:8000/api/v1/contacts';  // URL to web api

  constructor(
    private http: Http) { }


  getUsers(): Observable<contact[]> {
    return this.http.get(this.usersUrl).map((res: Response) => res.json())
    .catch(this.handleError);
  }

  addContact (contact: Object): Observable<contact[]> {
    console.log(contact);
    //const newUser = Object.assign({},contact);
    return this.http.post(this.usersUrl, contact).map((res:Response) =>res.json())
    .catch((error:any) => Observable.throw( {message:"Error del servidor"}));
  }

  getContact(id:string):Observable<contact[]>{
    return this.http.get(this.usersUrl+'/'+id).map((res: Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || {message:"Error del servidor"}));
  }

  updateContact(contact: Object):Observable<contact[]>{
    const url =`${this.usersUrl}/${contact["id"]}`;
    return this.http.put(url, contact).map((res: Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || {message:"Error del servidor"}));
  }

  deleteContact(contact: Object):Observable<contact[]>{
    const url =`${this.usersUrl}/${contact["id"]}`;
    return this.http.delete(url, contact).map((res: Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || {message:"Error del servidor"}));
  }
  private handleError (error: any){
    console.error(error);
    return Observable.throw(error);
  }



}
