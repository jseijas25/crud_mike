import { contact } from './../contact';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent {

  @Input() h1Text: string;
  @Input() submitText: string;
  @Input() contact: contact;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  submit(){
    this.onSubmit.emit(this.contact);
  }

}
