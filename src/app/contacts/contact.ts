export class contact {
  id?:number|string;
  name?: string;
  company?: string;
  email?: string;
  telephone?: number;
  notes?: string;
}
