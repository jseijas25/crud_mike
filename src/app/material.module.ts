import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule,
        MatToolbarModule,
        MatFormFieldModule, MatTableModule,
        MatInputModule, MatCardModule,
          MatSortModule,
       } from '@angular/material';

@NgModule({
  imports: [  MatSortModule,
  MatTableModule,MatButtonModule,MatToolbarModule, MatFormFieldModule, MatInputModule,MatCardModule, MatTableModule],
  exports: [  MatSortModule,
  MatTableModule,MatButtonModule, MatToolbarModule,MatFormFieldModule,MatInputModule,MatCardModule, MatTableModule],
})
export class MaterialModule { }
