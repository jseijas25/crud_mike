import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService{
  createDb(){
    const contacts =[
      {'id':'1','name': 'Jose', 'lastname': 'seijas'},
      {'id':'2','name': 'Michael', 'lastname': 'De Abreu'},
      {'id':'3','name': 'Jose', 'lastname': 'seijas'}
    ]
    return {contacts};
  }
}
